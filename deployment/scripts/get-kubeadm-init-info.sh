#!/bin/bash

set -euo pipefail

# Extract input variables to external data source
eval "$(jq -r '@sh "IP_ADDRESS=\(.ip_address) PRIVATE_KEY=\(.private_key)"')"

# Fetch data
token=$(/usr/bin/ssh cloud@$IP_ADDRESS -o "IdentityFile $PRIVATE_KEY" -o 'StrictHostKeyChecking no' -o 'UserKnownHostsFile /dev/null' "sudo kubeadm token list | grep -v DESCRIPTION | grep 'system:bootstrappers:kubeadm:default-node-token' | head -n1 | awk '{print \$1}'" || echo "NULL")
certhash=$(/usr/bin/ssh cloud@$IP_ADDRESS -o "IdentityFile $PRIVATE_KEY" -o 'StrictHostKeyChecking no' -o 'UserKnownHostsFile /dev/null' "openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'" || echo "NULL")

# Publish data as json string
jq -n --arg token "$token" --arg certhash "$certhash" '{"token":$token, "certhash":$certhash}'