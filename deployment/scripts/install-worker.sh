#!/bin/bash

set -euo pipefail

# Add local DNS alias if not exists
grep -qxF "${K8SCP_IP} k8scp" /etc/hosts || echo "${K8SCP_IP} k8scp" | sudo tee -a /etc/hosts
