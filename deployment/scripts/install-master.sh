#!/bin/bash

set -euo pipefail

# Add local DNS alias if not exists
grep -qxF "$(hostname -I) k8scp" /etc/hosts || echo "$(hostname -I) k8scp" | sudo tee -a /etc/hosts

# Create kubeadm config
cat <<EOF | sudo tee kubeadm-config.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: 1.25.1
controlPlaneEndpoint: "k8scp:6443"
networking:
  podSubnet: 10.0.0.0/16
EOF

# Install kubernetes
sudo kubeadm init --config=kubeadm-config.yaml --upload-certs | tee kubeadm-init.out