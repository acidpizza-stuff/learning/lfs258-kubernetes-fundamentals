locals {
  proxmox_node     = "pve"
  proxmox_host     = "pve.lan"
  proxmox_username = "root"
  proxmox_realm    = "pam"
}

module "k8scp" {
  count = 1

  source = "git::https://gitlab.com/acidpizza-stuff/infra/terraform-modules/proxmox-vm-terraform-module.git//module/ubuntu"

  # --- Proxmox Settings ---
  proxmox_node = local.proxmox_node

  # --- VM Settings ---
  vm_name = "k8scp-${count.index}"
  vm_description = "kubernetes learning"

  vm_cores = 2
  vm_memory = 8
  vm_disks = [ "50G" ]

  # --- VM Post Config ---
  user_public_key_path = var.user_public_key_path
}

module "k8sworker" {
  count = 1

  source = "git::https://gitlab.com/acidpizza-stuff/infra/terraform-modules/proxmox-vm-terraform-module.git//module/ubuntu"

  # --- Proxmox Settings ---
  proxmox_node = local.proxmox_node

  # --- VM Settings ---
  vm_name = "k8sworker-${count.index}"
  vm_description = "kubernetes learning"

  vm_cores = 2
  vm_memory = 8
  vm_disks = [ "50G" ]

  # --- VM Post Config ---
  user_public_key_path = var.user_public_key_path
}

resource null_resource "k8s_installation_common" {
  for_each = toset(concat(
    [ for vm in module.k8scp     : vm.ip ],
    [ for vm in module.k8sworker : vm.ip ],
  ))

  depends_on = [
    module.k8scp,
    module.k8sworker,
  ]
  connection {
    type = "ssh"
    user = "cloud"
    private_key = file(var.user_private_key_path)
    host = each.value
    script_path = "/home/cloud/k8s_installation_common.sh"
  }
  provisioner "remote-exec" {
    script = "scripts/install-common.sh"
  }
}

resource null_resource "k8s_installation_master" {
  for_each = toset(concat(
    [ for vm in module.k8scp : vm.ip ],
  ))
  
  depends_on = [
    null_resource.k8s_installation_common,
  ]
  connection {
    type = "ssh"
    user = "cloud"
    private_key = file(var.user_private_key_path)
    host = each.value
    script_path = "/home/cloud/k8s_installation_master.sh"
  }
  provisioner "remote-exec" {
    script = "scripts/install-master.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "set -eu",
      # Install calico network plugin manifest
      <<-EOT
        cat <<"EOF" | sudo KUBECONFIG=/etc/kubernetes/admin.conf kubectl apply -f -
        ${file("manifests/calico.yaml")}
        EOF
      EOT
    ]
  }
  provisioner "remote-exec" {
    # Need to copy kubeconfig to unprivileged location for scp
    inline = [
      "set -eu",
      "sudo cp /etc/kubernetes/admin.conf /tmp/admin.conf",
      "sudo chown cloud /tmp/admin.conf",
    ]
  }
  provisioner "local-exec" {
    # Copy kubeconfig to local machine
    command = "scp -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null cloud@${module.k8scp[0].ip}:/tmp/admin.conf kubeconfig"
  }
}

data "external" "kubeadm-init-info" {
  depends_on = [
    null_resource.k8s_installation_master
  ]

  program = ["/usr/bin/bash", "${path.module}/scripts/get-kubeadm-init-info.sh"]
  query = {
    ip_address = module.k8scp[0].ip
    private_key = var.user_private_key_path
  }
}

resource null_resource "k8s_installation_worker" {
  for_each = toset(concat(
    [ for vm in module.k8sworker : vm.ip ],
  ))
  
  depends_on = [
    null_resource.k8s_installation_master,
  ]
  connection {
    type = "ssh"
    user = "cloud"
    private_key = file(var.user_private_key_path)
    host = each.value
    script_path = "/home/cloud/k8s_installation_worker.sh"
  }

  # Workaround to add env variables to scripts
  provisioner "remote-exec" {
    inline = [
      "set -eu",
      # Add local DNS alias if not exists
      "grep -qxF \"${module.k8scp[0].ip} k8scp\" /etc/hosts || echo \"${module.k8scp[0].ip} k8scp\" | sudo tee -a /etc/hosts",
      # Join cluster
      <<-EOT
        if [ "${data.external.kubeadm-init-info.result.token}" = "" ]; then
          echo "ERROR: Token is empty. Need to recreate token on control plane with: sudo kubeadm token create. Also need to taint external data."
          exit 1
        fi
      EOT
      ,
      "sudo kubeadm join k8scp:6443 --token ${data.external.kubeadm-init-info.result.token} --discovery-token-ca-cert-hash sha256:${data.external.kubeadm-init-info.result.certhash}",
    ]
  }
  # provisioner "remote-exec" {
  #   script = "scripts/install-worker.sh"
  # }
}