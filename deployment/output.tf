output "ip" {
  value = concat(
    [ for vm in module.k8scp     : vm.ip ],
    [ for vm in module.k8sworker : vm.ip ],
  )
}

output "k8scp_ip" {
  value = module.k8scp[0].ip
}