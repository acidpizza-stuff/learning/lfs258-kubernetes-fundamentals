terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9.11"
    }
  }
}

provider "proxmox" {
  pm_api_url = "https://${local.proxmox_host}:8006/api2/json"
  pm_user = "${local.proxmox_username}@${local.proxmox_realm}"
  pm_password = var.proxmox_password
  pm_tls_insecure = true

  # pm_log_enable = true
  # pm_log_file="terraform-plugin-proxmox.log"
  # pm_debug = true
  # pm_log_levels = {
  #   _default = "debug"
  #   _capturelog = ""
  # }
}
