# Kubernetes Fundamentals LFS258

## Useful Paths

- `/etc/kubernetes/admin.conf`: The kubeconfig file for this cluster
- `/etc/kubernetes/manifests`: kubelet starts all pods here on startup
- `/var/lib/kubelet/config.yaml`: kubelet config


## Installation

### Pre-installation Configuration

1. Ensure the calico network plugin configuration at `manifests/calico.yaml` has the correct value for `CALICO_IPV4POOL_CIDR`.

The default CIDR range is `192.168.0.0/16`. Since this will conflict with the nodes IP range, we change this to `10.0.0.0/16`.


### Install and grow k8s cluster

1. Spin up nodes via terraform and run post-install scripts.

```bash
cd deployment/

# Need to run in 2 steps as terraform require the IP outputs to be ready
make apply-initial

make apply
```

2. Kubeconfig file from the control plane node at `/etc/kubernetes/admin.conf` will be copied to your local working directory as the file `kubeconfig`.

3. Untaint control plane nodes to allow running workloads.

```bash
kubectl taint nodes --all node-role.kubernetes.io/control-plane-
```

4. Update containerd configuration (`/etc/crictl.yaml`) - Not necessary?

```bash
sudo crictl config --set runtime-endpoint=unix:///run/containerd/containerd.sock --set image-endpoint=unix:///run/containerd/containerd.sock
```


### Grow k8s cluster

1. From worker node, use join command that was output from master control plane node. It is only valid for 23 hours. After 23 hours, a new one can be regenerated from the control plane node.

```bash
### On master control plane node ###
# Create token if expired already (TOKEN)
sudo kubeadm token list
sudo kubeadm token create --print-join-command

# Get cert hash (CERT_HASH)
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'

# Create control plane certificate (Get certifificate key as CERT_KEY)
sudo kubeadm init phase upload-certs --upload-certs

### Grow worker node ###
# Join cluster
TOKEN="..."
CERT_HASH="..."
sudo kubeadm join k8scp:6443 --token ${TOKEN} --discovery-token-ca-cert-hash sha256:${CERT_HASH}

### Grow control plane node ###
# Join cluster
TOKEN="..."
CERT_HASH="..."
CERT_KEY="..."
sudo kubeadm join k8scp:6443 --token ${TOKEN} --discovery-token-ca-cert-hash sha256:${CERT_HASH} --control-plane --certificate-key ${CERT_KEY}
```

## Maintenance

### Backup etcd

```bash
# Find data directory of etcd daemon -> /var/lib/etcd
sudo grep data-dir /etc/kubernetes/manifests/etcd.yaml

# In order to use TLS, need 3 files: ca.crt, server.crt, server.key
kubectl -n kube-system exec -it etcd-k8scp-0 -- sh
cd /etc/kubernetes/pki/etcd
# Cannot use ls so use echo * instead.
echo *
exit

# Check health of database
kubectl -n kube-system exec -it etcd-k8scp-0 -- sh -c \
  "ETCDCTL_API=3 \
  ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
  ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
  ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
  etcdctl endpoint health"

# Check number of databases
kubectl -n kube-system exec -it etcd-k8scp-0 -- sh -c \
  "ETCDCTL_API=3 \
  ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
  ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
  ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
  etcdctl --endpoints=https://127.0.0.1:2379 member list -w table"

# Save snapshot to /var/lib/etcd/snapshot.db
# /var/lib/etcd is bind mounted to host so snapshot.db will be available from controlplane node
kubectl -n kube-system exec -it etcd-k8scp-0 -- sh -c \
  "ETCDCTL_API=3 \
  ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
  ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
  ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
  etcdctl --endpoints=https://127.0.0.1:2379 snapshot save /var/lib/etcd/snapshot.db"

# Verify snapshot
kubectl -n kube-system exec -it etcd-k8scp-0 -- sh -c \
  "ETCDCTL_API=3 \
  ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
  ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
  ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
  etcdctl --write-out=table snapshot status /var/lib/etcd/snapshot.db"

# Backup files - snapshot, kubeadm setup config, etcd keys and certs
mkdir $HOME/backup
sudo cp /var/lib/etcd/snapshot.db $HOME/backup/snapshot.db-$(date +%y-%m-%d)
sudo cp ~/kubeadm-config.yaml $HOME/backup/
sudo cp -r /etc/kubernetes/pki/etcd $HOME/backup
```

### Restore etcd

```bash
# Stop all control plane components by moving out all static pods
cd /etc/kubernetes
mkdir test
cd manifests
mv * ../test

# Backup existing etcd data dir and create a new one
mv /var/lib/etcd/ /var/lib/etcd-backup
mkdir /var/lib/etcd

# Restore using native etcdctl binary
ETCDCTL_API=3 \
  ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt \
  ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt \
  ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key \
  etcdctl --endpoints=https://127.0.0.1:2379 --data-dir /var/lib/etcd/ snapshot restore /tmp/snapshot.db

# Restart all control plane components by moving manifests back in to static pods directory.
cd /etc/kubernetes/manifests
mv ../test/* .
rm -rf ../test
```

### Upgrade Cluster

Need to perform these steps for each node.

```bash
# Update kubeadm to desired version
sudo apt update
sudo apt-cache madison kubeadm
sudo apt-mark unhold kubeadm
sudo apt-get install -y kubeadm=1.26.1-00
sudo apt-mark hold kubeadm

# Upgrade controlplane node kubeadm
kubectl drain k8scp-0 --ignore-daemonsets
sudo kubeadm upgrade plan
sudo kubeadm upgrade apply v1.26.1

# Upgrade controlplane node kubelet and kubectl
sudo apt-mark unhold kubelet kubectl
sudo apt-get install -y kubelet=1.26.1-00 kubectl=1.26.1-00
sudo apt-mark hold kubelet kubectl

# Restart services
sudo systemctl daemon-reload
sudo systemctl restart kubelet
kubectl uncordon k8scp-0
```

## HA

Create a VM to act as a load balancer.

```bash
# Install haproxy
sudo apt update
sudo apt install -y haproxy vim

# Update backend servers IP (get template at deployment/haproxy/haproxy.cfg)
vim /etc/haproxy/haproxy.cfg

# Restart haproxy
sudo systemctl restart haproxy.service
sudo systemctl enable haproxy.service
```